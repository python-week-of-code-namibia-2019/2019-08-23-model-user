# Python Week Of Code, Namibia 2019

[blog/models.py](blog/models.py) has the source code of the database model.

## Task for Instructor

1. Add

   ```
   author = models.ForeignKey(
       settings.AUTH_USER_MODEL,
       on_delete=models.CASCADE,
   )
   ```

   to [blog/models.py](blog/models.py).

## Tasks for Learners

1. Create your database with

   ```
   python manage.py migrate
   ```
2. Create a super user with

   ```
   python manage.py createsuperuser
3. Run the server with

   ```
   python manage.py runserver
   ```
4. Create two new users.
5. Create 2 blog posts for each user.